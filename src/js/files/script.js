
import { getRandom, getDigFormat, addMoney, addResource, deleteMoney, shuffle, addRemoveClass } from '../files/functions.js';
import { startData } from './startData.js';


const inpName = document.querySelectorAll('[data-input="name"]');
const inpAge = document.querySelectorAll('[data-input="age"]');

if (document.querySelector('.wrapper')) {
	if (localStorage.getItem('user-name') && localStorage.getItem('user-age')) {
		if (document.querySelector('.wrapper').classList.contains('_start-screen')) {
			document.querySelector('.wrapper').classList.remove('_start-screen');
		}
	} else {
		document.querySelector('.wrapper').classList.add('_start-screen');
	}

	if (localStorage.getItem('moved-privacy') == 1) {
		document.querySelectorAll('.app-main__title')[0].classList.remove('_tab-active');
		document.querySelectorAll('.app-main__title')[3].classList.add('_tab-active');
		setTimeout(() => {
			localStorage.setItem('moved-privacy', 0);
		}, 1000);
	}
}


export function initStartData() {

	if (!localStorage.getItem('money')) {
		localStorage.setItem('money', startData.bank);
	}
	writeScore();

	// if (!localStorage.getItem('resource')) {
	// 	localStorage.setItem('resource', 0);
	// }
	// writeResource();


	if (!localStorage.getItem('current-bet')) {
		localStorage.setItem('current-bet', startData.countBet);
	}
	writeBet();

	// if (!localStorage.getItem('level')) {
	// 	localStorage.setItem('level', 1);
	// }

}

export function writeDataUser() {
	if (localStorage.getItem('user-name')) {
		inpName.forEach(item => item.value = localStorage.getItem('user-name'));
	}
	if (localStorage.getItem('user-age')) {
		inpAge.forEach(item => item.value = localStorage.getItem('user-age'));
	}
}
writeDataUser();

function writeScore() {
	if (document.querySelector('.score')) {
		let money = getDigFormat(+localStorage.getItem('money'));
		document.querySelectorAll('.score').forEach(el => {
			el.textContent = money;
		})
	}
}

// function writeResource() {
// 	if (document.querySelector('.resource')) {
// 		let money = getDigFormat(+localStorage.getItem('resource'));
// 		document.querySelectorAll('.resource').forEach(el => {
// 			el.textContent = money;
// 		})
// 	}
// }

export function writeBet() {
	if (document.querySelector(startData.nameItemBet)) {
		document.querySelectorAll(startData.nameItemBet).forEach(el => {
			el.textContent = localStorage.getItem('current-bet');
		})
	}
}



initStartData();

//========================================================================================================================================================
// Функция присвоения случайного класса анимациии money icon
const anim_items = document.querySelectorAll('.icon-game');

function getRandomAnimate() {
	let number = getRandom(0, 3);
	let arr = ['jump', 'scale', 'rotate'];
	let random_item = getRandom(0, anim_items.length);
	anim_items.forEach(el => {
		if (el.classList.contains('_anim-icon-jump')) {
			el.classList.remove('_anim-icon-jump');
		} else if (el.classList.contains('_anim-icon-scale')) {
			el.classList.remove('_anim-icon-scale');
		} else if (el.classList.contains('_anim-icon-rotate')) {
			el.classList.remove('_anim-icon-rotate');
		}
	})
	setTimeout(() => {
		anim_items[random_item].classList.add(`_anim-icon-${arr[number]}`);
	}, 100);
}

if (anim_items.length) {
	setInterval(() => {
		getRandomAnimate();
	}, 20000);
}



//========================================================================================================================================================
export function showFinalScreen(score) {
	const final = document.querySelector('.final');
	const finalScore = document.querySelector('.final__score');

	finalScore.textContent = `+${score}`;

	setTimeout(() => {
		final.classList.add('_visible');
	}, 50);
}
