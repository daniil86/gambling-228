
import { addMoney, deleteMoney, noMoney, addRemoveClass } from './functions.js';
import { } from './script.js';
import { startData } from './startData.js';


// Объявляем слушатель событий "клик"
document.addEventListener('click', (e) => {

	const wrapper = document.querySelector('.wrapper');

	const targetElement = e.target;

	const money = +localStorage.getItem('money');
	const bet = +localStorage.getItem('current-bet');

	// privacy screen
	if (targetElement.closest('.preloader__button')) {
		location.href = 'main.html';
	}

	// main screen

	if (targetElement.closest('[data-button="privacy"]')) {
		location.href = 'index.html';
	}

	if (targetElement.closest('[data-button="news-screen"]')) {
		wrapper.classList.add('_news-screen');
	}
	if (targetElement.closest('[data-button="news-screen-home"]')) {
		wrapper.classList.remove('_news-screen');
	}

})





